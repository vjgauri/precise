
import UIKit

///Custom colors
extension UIColor {
    
    @nonobjc static let appBackgroundColor = UIColor(red: 240.0/255.0, green: 242.0/255.0, blue: 239.0/255.0, alpha: 1)
    @nonobjc static let navigationColor = UIColor(red: 0.0/255.0, green: 143.0/255.0, blue: 214.0/255.0, alpha: 1)
}

// Custom Fonts
