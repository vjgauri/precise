
import UIKit

class DashboardCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "dashboardMenuCell"
    @IBOutlet var menuTitleLabel: UILabel!
    @IBOutlet var menuImageView: UIImageView!
    
    func configureCell(_ menu: [String: String]) {
        
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.menuTitleLabel.textColor = UIColor.darkGray
        self.menuImageView.image = UIImage(named: menu["Image"]!)
        self.menuTitleLabel.text = menu["Menu"]
    }
    
}
