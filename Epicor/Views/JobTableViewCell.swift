

import UIKit

class JobTableViewCell: UITableViewCell {

    static let identifier = "JobTableViewCell"

    @IBOutlet var jobIdLabel: UILabel!
    @IBOutlet var jobDescriptionLabel: UILabel!
    @IBOutlet var dueDateLabel: UILabel!
    @IBOutlet var status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(_ job: Job) {

    }

}
