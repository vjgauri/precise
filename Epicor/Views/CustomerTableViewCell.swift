//
//  CustomerTableViewCell.swift
//  Epicor
//
//  Created by King on 27/04/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit

class CustomerTableViewCell: UITableViewCell {
    
    static let identifier = "CustomerTableViewCell"
    
    
    @IBOutlet weak var grpText: UITextView!
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
