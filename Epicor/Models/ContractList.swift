

import Foundation

struct ContractResponse: Codable {
    let data: [Contract]?
}

struct Contract: Codable {
    
    let contractId: String?
    let revision: String?
    let description: String?
    let contractReference: String?
    let contractStatus: String?
    let sdate: String?
    let edate: String?
    let cname: String?
    let address: String?
    let attn: String?
    let phone: String?
    let fax: String?
    let contractType: String?
    let territory: String?
    let industryClassType: String?
    let industryClassCode: String?
    let industryClass: String?
    let contractManager: String?
    let salesRepresentative: String?
    let primarySalesperson: String?
    let totalContractValue: String?
    let totalInvoicedAmount: String?
    let totalRetentionAmount: String?
    let userId: String?
    
    private enum CodingKeys : String, CodingKey {
 
        case contractId = "contract_id"
        case revision = "revision"
        case description = "description"
        case contractReference = "contract_reference"
        case contractStatus = "contract_status"
        case sdate = "sdate"
        case edate = "edate"
        case cname = "cname"
        case address = "address"
        case attn = "attn"
        case phone = "phone"
        case fax = "fax"
        case contractType = "contract_type"
        case territory = "territory"
        case industryClassType = "industry_class_type"
        case industryClassCode = "industry_class_code"
        case industryClass = "industry_class"
        case contractManager = "contract_manager"
        case salesRepresentative = "sales_representative"
        case primarySalesperson = "primary_salesperson"
        case totalContractValue = "total_contract_value"
        case totalInvoicedAmount = "total_invoiced_amount"
        case totalRetentionAmount = "total_retention_amount"
        case userId = "user_id"
    }
}
