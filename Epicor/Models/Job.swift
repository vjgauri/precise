//
//  Job.swift
//  Epicor
//
//  Created by King on 27/04/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import Foundation

struct Job {
    let jobTitle: String?
    let jobSubTitle: String?
}
