
import Foundation

struct CustomerResponse: Codable {
    let value: [Customer]?
}

struct Customer: Codable {
    
    let customerCustID: String?
    let customerName: String?
    let customerCustomerType: String?

    
    enum CodingKeys: String, CodingKey {
        case customerCustID = "Customer_CustID"
        case customerName = "Customer_Name"
        case customerCustomerType = "Customer_CustomerType"
    }
}
