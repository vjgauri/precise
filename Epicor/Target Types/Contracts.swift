

import Moya

enum ContractType {
    case getContracts(userId: String, contractStatus: String)
}

extension ContractType: TargetType {
    var baseURL: URL {
        return URL(string: Constants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .getContracts: return "webservice.php"
        }
    }
    
    var method: Method {
        switch self {
        case .getContracts: return .post
        }
    }
    
    var sampleData: Data { return Data()}
    
    var task: Task {
        switch self {
        case .getContracts(let userId, let contractStatus):
            return .requestParameters(parameters: ["user_id": userId,"contract_status": contractStatus], encoding: JSONEncoding.prettyPrinted)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    
}
