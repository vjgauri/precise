

import Moya

enum CustomerType {
    case getCustomers(afterDate: String, afterTime: String)
}

extension CustomerType: TargetType {
    
    var baseURL: URL {
        return URL(string: Constants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .getCustomers:
            return "ERP10/api/v1/BaqSvc/zCRM-Customers/"
        }
    }
    
    var method: Method {
        switch self {
        case .getCustomers:
            return .get
            
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getCustomers:
            return Data()
        }
    }
    
    var task: Task {
        switch self {
        case .getCustomers(let afterDate, let afterTime):
            return .requestParameters(parameters: ["?AfterDate": afterDate,
                                                   "AfterTime": afterTime], encoding: URLEncoding.default)
        }
        
    }
    
    var headers: [String : String]? {
        return ["Authorization": "Basic bWFuYWdlcjpkZW1vMjQ2OA==",
                "AfterDate": "","AfterTime": ""]
    }

}
