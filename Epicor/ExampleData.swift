//
//  ExampleData.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 8/1/17.
//  Copyright © 2017 Yong Su. All rights reserved.
//

import Foundation

//
// MARK: - Section Data Structure
//
public struct Item {
    var name: String
    var detail: String
    var due:String = ""
    var status:String = ""
    
    public init(name: String, detail: String) {
        self.name = name
        self.detail = detail
    }
    
    public init(name: String, detail: String, due:String, status: String) {
        self.name = name
        self.detail = detail
        self.due  = due
        self.status = status
    }
}

public struct Section {
    var name: String
    var items: [Item]
    var collapsed: Bool
    
    public init(name: String, items: [Item], collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

public var sectionsData: [Section] = [
    
    Section(name: "Contacts", items: [
        Item(name: "Name", detail: "Position"),
        Item(name: "Name", detail: "Position")
    ]),
    Section(name: "Jobs", items: [
        Item(name: "Name", detail: "Position"),
        Item(name: "Name", detail: "Position")
        ]),
    Section(name: "Calls", items: [

        ]),
    Section(name: "Tasks", items: [

        ]),
    Section(name: "Ship To / Site Addresses", items: [

        ]),
    Section(name: "Quotes", items: [
 
        ]),
    Section(name: "Contracts", items: [

        ]),
    Section(name: "Orders", items: [

        ]),
    Section(name: "Projects", items: [
  
        ]),
    Section(name: "Invoices", items: [

        ]),
]
