
import Foundation

struct Constants {
    
    static let baseURL: String = "https://crmdemo.precisebusiness.com.au/"
    
    static let menuOptionList : [String] = ["Home", "Favorites","Customers", "Cases", "Tasks", "Contacts" , "Quotes", "Contracts", "Project", "Jobs", "Schedule", "Reports", "Settings"]
}
