

import Foundation

enum UserType {
    case Employee
    case SubContractor
}

struct DashboardGridMenu {
    
    static let shared = DashboardGridMenu()

    static let employeeMenuTitles = ["Contract","Customer","Contact Person","Activities","Task"]
    static let employeeMenuImages = ["contract","customer","contact","activities","task"]
    static let contractorMenuTitles = ["My Work Orders","My Activities", "My Tasks"]
    static let contractorMenuImages = ["work","activities","task"]
    
    static func getGridMenu( userType: UserType ) -> [[String: String]] {
        switch userType {
        case .Employee:
            var menuList: [[String: String]] = []
            for i in 0...(employeeMenuTitles.count-1) {
                menuList.append(["Menu": employeeMenuTitles[i],"Image": employeeMenuImages[i]])
            }
            return menuList
        case .SubContractor:
            var menuList: [[String: String]] = []
            for i in 0...(contractorMenuTitles.count-1) {
                menuList.append(["Menu": contractorMenuTitles[i],"Image": contractorMenuImages[i]])
            }
            return []
        }
    }
    
}
