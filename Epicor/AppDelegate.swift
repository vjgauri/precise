//
//  AppDelegate.swift
//  Epicor
//
//  Created by King on 25/04/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftMessageBar
import SideMenuSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        
        // App initialization
        setNavigationBarColor()
        configureSideMenu()
        
        let config = SwiftMessageBar.Config.Builder()
            .withErrorColor(.red)
            .withSuccessColor(.blue)
            .withTitleFont(.boldSystemFont(ofSize: 20))
            .withMessageFont(.systemFont(ofSize: 14))
            .withHapticFeedbackEnabled(true)
            .build()
        SwiftMessageBar.setSharedConfig(config)
        return true
    }
    
    private func configureSideMenu() {
        SideMenuController.preferences.basic.menuWidth = 280
        SideMenuController.preferences.basic.defaultCacheKey = "0"
    }
}

extension AppDelegate {
    
    func setNavigationBarColor() {
        
        UINavigationBar.appearance().barTintColor = .navigationColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = true
    }
}

