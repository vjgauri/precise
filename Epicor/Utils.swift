//
//  Utils.swift
//  Epicor
//
//  Created by VijayKumar Dogra on 27/04/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit
import MBProgressHUD

class Utils: NSObject {
    static private var hud:MBProgressHUD?
    
    static func showHUD(view:UIView){
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud!.mode = MBProgressHUDMode.indeterminate
        hud!.label.text = "Please Wait..."
        hud!.removeFromSuperViewOnHide = true
        
        hud!.isSquare = true
        hud!.bezelView.backgroundColor = .init(red: 133/255, green: 186/255, blue: 85/255, alpha: 1)
        hud!.backgroundView.blurEffectStyle = .dark
        hud!.contentColor = .white
        
        hud!.label.textColor = .white
        hud!.bezelView.color = .black
    }
    
    static func hideHUD(){
        hud?.hide(animated: true)
    }
}
