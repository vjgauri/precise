
import Alamofire
import SwiftyJSON


class JobsService: NSObject {
    private let headers = [
        "Authorization": "Basic bWFuYWdlcjpkZW1vMjQ2OA=="
    ]
    static let shared = JobsService()
    
    func getJobs(url:String, successHandler:@escaping([JSON]) -> Void, failureHandler:@escaping(String) -> Void) {
        var strUrl:String = url
        if url.isEmpty{
            strUrl = "https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobList"
        }
        
        Alamofire.request(strUrl, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (responseData) in
            
            if responseData.error == nil {
                
                guard let response = responseData.result.value as? [String:Any] else {
                    return
                }
                if response["value"] != nil {
                    let jsonResponseValue = JSON(response["value"]!)
                    guard let jobsArray = jsonResponseValue.array else {
                        return failureHandler("No Data Found")
                    }
                    successHandler(jobsArray)
                } else {
                    failureHandler("No Data Found")
                }
            } else {
                failureHandler(responseData.error?.localizedDescription ?? "Failed to get Jobs")
            }
        }
    }
    
    func getJobDetails(jobNumber: String,
                       successHandler: @escaping([JSON]) -> Void,
                       failureHandler: @escaping(String) -> Void) {
        let url = "https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobList/?$filter=JobHead_JobNum%20eq%20'" + jobNumber + "'"

        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (responseData) in
            
            if responseData.error == nil {
                
                guard let response = responseData.result.value as? [String:Any] else {
                    return
                }
                if response["value"] != nil {
                    let jsonResponseValue = JSON(response["value"]!)
                    guard let jobsArray = jsonResponseValue.array else {
                        return failureHandler("No Data Found")
                    } 
                    successHandler(jobsArray)
                } else {
                    failureHandler("No Data Found")
                }
            } else {
                failureHandler(responseData.error?.localizedDescription ?? "No Data Found")
            }
        }
    }
    
    func getTasksAndOperations(jobNumber: String,
                               successHandler: @escaping([JSON]) -> Void,
                               failureHandler: @escaping(String) -> Void) {
  
        let url = "https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobOper/?$filter=JobHead_JobNum%20eq%20'" + jobNumber + "'"
        
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (responseData) in
            
            if responseData.error == nil {
                
                guard let response = responseData.result.value as? [String:Any] else {
                    return
                }
                if response["value"] != nil {
                    let jsonResponseValue = JSON(response["value"]!)
                    guard let jobsArray = jsonResponseValue.array else {
                     return failureHandler("No Data Found")
                     }
                    successHandler(jobsArray)
                } else {
                    failureHandler("No Data Found")
                }
            } else {
                failureHandler(responseData.error?.localizedDescription ?? "No Data Found")
            }
        }
    }
    
    func getDataForNewJob(successHandler: @escaping([JSON]) -> Void,
                          failureHandler: @escaping(String) -> Void) {
        
        Alamofire.request("https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobList/GetNew", method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (responseData) in
            
            if responseData.error == nil {
                
                guard let response = responseData.result.value as? [String:Any] else {
                    return
                }
                if response["value"] != nil {
                    let jsonResponseValue = JSON(response["value"]!)
                    guard let jobsArray = jsonResponseValue.array else {
                        return failureHandler("No Data Found")
                    }
                    successHandler(jobsArray)
                } else {
                    failureHandler("No Data Found")
                }
            } else {
                failureHandler(responseData.error?.localizedDescription ?? "No Data Found")
            }
        }
    }
    
    func createNewJob(parameters: [String: Any],
                      successHandler: @escaping([JSON]) -> Void,
                      failureHandler: @escaping(String) -> Void) {
        
        print(parameters)
        
        Alamofire.request("https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobList", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseData) in
            
            if responseData.error == nil {
                
                guard let response = responseData.result.value as? [String:Any] else {
                    return
                }
                if response["value"] != nil {
                    let jsonResponseValue = JSON(response["value"]!)
                    guard let jobsArray = jsonResponseValue.array else {
                        return failureHandler("No Data Found")
                    }
                    successHandler(jobsArray)
                } else {
                    failureHandler("No Data Found")
                }
            } else {
                failureHandler(responseData.error?.localizedDescription ?? "No Data Found")
            }
        }
    }
}
