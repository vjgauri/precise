
import Moya

class ContractService {
    
    static let shared = ContractService()
    private let provider = MoyaProvider<ContractType>()
    
    func getContracts(userId: String,
                      contractStatus: String,
                      successHandler: @escaping([Contract]?) -> Void,
                      failureHandler: @escaping(String) -> Void) {
        
        provider.request(.getContracts(userId: userId,
                                       contractStatus: contractStatus)) { (result) in
                                        
                                        switch result {
                                        case .success(let value):
                                            
                                            do {
                                                let contracts = try JSONDecoder().decode(ContractResponse.self, from: value.data)
                                                successHandler(contracts.data)
                                            } catch {
                                            failureHandler(error.localizedDescription)
                                            }
                                            
                                            print(value)
                                        case .failure(let error):
                                            print(error)
                                            failureHandler(error.localizedDescription)
                                        }
        }
    }
}
