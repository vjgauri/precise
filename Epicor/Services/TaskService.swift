
import Alamofire
import SwiftyJSON
import Foundation

class TaskService: NSObject {
    private let headers = [
        "Authorization": "Basic bWFuYWdlcjpkZW1vMjQ2OA=="
    ]
    static let shared = TaskService()
    
    func getNewTaskData(successHandler:@escaping([JSON]) -> Void,
                        failureHandler:@escaping(String) -> Void) {
        
        Alamofire.request("https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobOper/GetNew", method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (responseData) in
            
            if responseData.error == nil {
                
                guard let response = responseData.result.value as? [String:Any] else {
                    return
                }
                if response["value"] != nil {
                    let jsonResponseValue = JSON(response["value"]!)
                    guard let jobsArray = jsonResponseValue.array else {
                        return failureHandler("No Data Found")
                    }
                    successHandler(jobsArray)
                } else {
                    failureHandler("No Data Found")
                }
            } else {
                failureHandler(responseData.error?.localizedDescription ?? "No Data Found")
            }
        }
    }
    
    func createNewTask(parameters: [String: Any],
                      successHandler: @escaping([JSON]) -> Void,
                      failureHandler: @escaping(String) -> Void) {
        
        print(parameters)
        
        Alamofire.request("https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobOper", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseData) in
            
            if responseData.error == nil {
                
                guard let response = responseData.result.value as? [String:Any] else {
                    return
                }
                if response["value"] != nil {
                    let jsonResponseValue = JSON(response["value"]!)
                    guard let jobsArray = jsonResponseValue.array else {
                        return failureHandler("No Data Found")
                    }
                    successHandler(jobsArray)
                } else {
                    failureHandler("No Data Found")
                }
            } else {
                failureHandler(responseData.error?.localizedDescription ?? "No Data Found")
            }
        }
    }
    
}
