
import Moya
import Alamofire

class CustomerService {
    
    static let shared = CustomerService()
    
    let provider = MoyaProvider<CustomerType>()
    
    func getCustomers(afterDate: String, afterTime: String,successHandler: @escaping ([Customer]) -> Void, failureHandler: @escaping(String) -> Void) {
        
        provider.request(.getCustomers(afterDate: afterDate, afterTime: afterTime)) { (result) in
            switch result {
            case .success(let value):
                print(value)
                do {
                    let data = try JSONDecoder().decode(CustomerResponse.self, from: value.data)
                    guard let customers = data.value else {
                        return failureHandler("Something went wrong")
                    }
                    successHandler(customers)
                } catch {
                    return failureHandler(error.localizedDescription)
                }
            case .failure( let error):
                return failureHandler(error.localizedDescription)
            }
        }
    }
}
