//
//  JobsTableViewCell.swift
//  Epicor
//
//  Created by king on 4/28/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit

class JobsTableViewCell: UITableViewCell {

    @IBOutlet var lbl_JobValue: UILabel!
     @IBOutlet var lbl_DescValue: UILabel!
     @IBOutlet var lbl_DescTaskValue: UILabel!
    @IBOutlet var vwJob_Desc: UIView!
    @IBOutlet var lbl_DueValue: UILabel!
    @IBOutlet var lbl_StatusValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
