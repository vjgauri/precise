//
//  JobsViewController.swift
//  Epicor
//
//  Created by king on 4/28/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Floaty

class JobsViewController: UIViewController, UISearchBarDelegate {

    @IBAction func didSettingClick(_ sender: Any) {

    }
    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var lblJob: UILabel!
     @IBOutlet var lblDesc: UILabel!
     @IBOutlet var lblStart: UILabel!
     @IBOutlet var lblDue: UILabel!
     @IBOutlet var lblStatus: UILabel!
    @IBOutlet var titileView: UIView!
    @IBOutlet var tableJob: UITableView!
    var jobs:[Any] = []
    var originalJobs:[Any] = []
    var isFromCustomer:Bool = false
    var strLblFilter:String = ""
    var strURL = "https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobList"

    override func viewDidLoad() {
        super.viewDidLoad()
       self.title = "Jobs"
       tableJob.register(UINib(nibName: "JobsTableViewCell", bundle: nil), forCellReuseIdentifier: "JobsTableViewCell")
        searchBar.delegate = self
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.singleTap(sender:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
       

        configuerNavigationBar()

        let floaty = Floaty()
        floaty.overlayColor = UIColor.gray.withAlphaComponent(0.9)
        floaty.buttonColor = UIColor.orange
        floaty.itemTitleColor = UIColor.orange
        floaty.itemShadowColor = UIColor.clear
        floaty.plusColor = UIColor.white
        
        floaty.itemImageColor = UIColor.clear
        floaty.itemSpace = 20
        floaty.itemSize = 30
        floaty.buttonShadowColor = UIColor.clear
        floaty.addItem("Create New Job", icon: UIImage(named: "orangearrow")!, handler: { item in
            let jobCreationViewController: JobCreationViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobCreationViewController") as! JobCreationViewController
            self.navigationController?.pushViewController(jobCreationViewController, animated: true)
            floaty.close()
        })
        self.view.addSubview(floaty)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(isFromCustomer){
            self.title = "Customer Jobs"
            lblMsg.text = strLblFilter
            lblMsg.isHidden = false
        }else{
            lblMsg.isHidden = true
            self.title = "Jobs"
        }
        getJobs()
    }
    
    func configuerNavigationBar() {
        let backButton = UIBarButtonItem(image: UIImage(named: "back")!, style: .plain, target: self, action: #selector(backButtonDidTap))
        let settings = UIBarButtonItem(image: UIImage(named: "SettingsBearing")!, style: .plain, target: self, action: #selector(commonSelector))
        let refresh = UIBarButtonItem(image: UIImage(named: "refresh")!, style: .plain, target: self, action: #selector(commonSelector))
        
        //self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.rightBarButtonItems = [settings,refresh]
    }
    
    @objc func backButtonDidTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func commonSelector() {
        
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        self.searchBar.resignFirstResponder()
    }
    
    func setMessage(strMsg:String){
        strLblFilter = strMsg
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var filterCustomers: [Any] = []
        
        if(searchText.isEmpty){
            self.jobs = self.originalJobs
            self.tableJob.reloadData()
            return
        }
        
        if(self.originalJobs.count <= 0){
            return
        }
        
        for i in 0...self.originalJobs.count-1 {
            let custDict:JSON = self.originalJobs[i] as! JSON
            var custName:String = custDict["JobHead_Status"].stringValue
            if(custName.range(of: searchText) != nil){
                filterCustomers.append(custDict)
                continue
            }
            
            custName = custDict["JobHead_JobNum"].stringValue
            if(custName.range(of: searchText) != nil){
                filterCustomers.append(custDict)
                continue
            }
        }
        
        self.jobs = filterCustomers
        self.tableJob.reloadData()
    }
    
    func getJobs() {
        
        Utils.showHUD(view: self.view)
        JobsService.shared.getJobs(url: strURL, successHandler: { (json) in
            DispatchQueue.main.async {
                Utils.hideHUD()
                self.jobs = json
                self.originalJobs = self.jobs
                self.tableJob.reloadData()
            }
        }) { (error) in
            Utils.hideHUD()
            print(error)
        }
    }
    
    @IBAction func MenuDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    func toStringOtherFormat(d:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: d)
        return (date?.toString(format: "dd MMM yyyy"))!
    }
    
    func callFromCustomer(strCusNum:String){
        isFromCustomer = true
        
        strURL = strCusNum
        let backButton = UIBarButtonItem(image: UIImage(named: "back")!, style: .plain, target: self, action: #selector(backButtonDidTap))
        self.navigationItem.leftBarButtonItem = backButton
    }

}

extension JobsViewController: UITableViewDelegate, UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobs.count
    }
    
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let jobCell: JobsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "JobsTableViewCell") as! JobsTableViewCell
    
        let jobDict: JSON = self.jobs[indexPath.row] as! JSON
        let jobId = jobDict["JobHead_JobNum"].stringValue
        if !jobId.isEmpty {
            jobCell.lbl_JobValue.text = "\(jobId)"
        }
        let jobName = jobDict["JobHead_CustName"].stringValue
        if !jobName.isEmpty {
            jobCell.lbl_DescValue.text = jobName
        }
    
        let jobDescription = jobDict["JobHead_PartDescription"].stringValue
        if !jobDescription.isEmpty {
            jobCell.lbl_DescTaskValue.text = jobDescription
        }
        let dueDate = jobDict["JobHead_DueDate"].stringValue
    
        if !dueDate.isEmpty {
            // let Converted_duedate = self.getDate(fromStr: dueDate)
            jobCell.lbl_DueValue.text = self.toStringOtherFormat(d: dueDate)
        }else{
            jobCell.lbl_DueValue.text = ""
        }
        let jobStatus = jobDict["JobHead_Status"].stringValue
        if jobStatus.isEmpty {
            jobCell.lbl_StatusValue.text = "Open"
        }
        return jobCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let jobCell: JobsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "JobsTableViewCell") as! JobsTableViewCell
        jobCell.lbl_JobValue.text = "Job #"
        jobCell.lbl_JobValue.textColor = UIColor.blue
        jobCell.lbl_DescValue.text = "Customer/Description"
        jobCell.lbl_DescValue.textColor = UIColor.blue
        jobCell.lbl_DueValue.text = "Due"
        jobCell.lbl_DueValue.textColor = UIColor.blue
        jobCell.lbl_StatusValue.text = "Status"
        jobCell.lbl_StatusValue.textColor = UIColor.blue
        return jobCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let jobDict: JSON = self.jobs[indexPath.row] as! JSON
        let jobId = jobDict["JobHead_JobNum"].stringValue
        
        let jobDetailsVC: JobDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: JobDetailsViewController.identifier) as! JobDetailsViewController
        jobDetailsVC.jobNum = jobId
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(jobDetailsVC, animated: true)
      
    }
    
}
