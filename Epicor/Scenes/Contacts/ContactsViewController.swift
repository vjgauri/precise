

import UIKit
import Alamofire
import SwiftyJSON

class ContactsViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet var customerTableView: UITableView!
    
    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    static let identifier = "ContactsViewController"
    var customers: [Any] = []
    var origCustomers:[Any] = []
    var isFromCustomer:Bool = false
    var strLblFilter:String = ""
    
    @IBOutlet weak var lblHtContsraint: NSLayoutConstraint!
    var strURL:String = "https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/zCRM-CustCnt/?AfterDate=01%2F01%2F1970&AfterTime=0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contacts"
        searchBar.delegate = self
    }
    
    @IBAction func btnMenuClicked(_ sender: Any) {
        if(isFromCustomer){
            self.navigationController?.popViewController(animated: true)
        }else{
            sideMenuController?.revealMenu()
        }
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        self.searchBar.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.singleTap(sender:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
        if(isFromCustomer){
            self.title = "Customer Contacts"
            lblFilter.text = strLblFilter
            lblFilter.isHidden = false
        }else{
            lblFilter.isHidden = true
            self.title = "Contacts"
            lblHtContsraint.constant = 0
        }
        callService()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var filterCustomers: [Any] = []
        
        if(searchText.isEmpty){
            self.customers = self.origCustomers
            self.customerTableView.reloadData()
            return
        }
        
        for i in 0...self.origCustomers.count-1 {
            let custDict:JSON = self.origCustomers[i] as! JSON
            let custName:String = custDict["CustCnt_Name"].stringValue
            if(custName.range(of: searchText) != nil){
                filterCustomers.append(custDict)
                continue;
            }
            var myString:String? = custDict["CustCnt_CellPhoneNum"].stringValue
            myString = myString?.replacingOccurrences(of: "-", with: "")
            myString = myString?.replacingOccurrences(of: ".", with: "")
            if(!myString!.isEmpty && ((myString?.range(of: searchText)) != nil)){
                filterCustomers.append(custDict)
                continue;
            }
            
            myString = custDict["CustCnt_PhoneNum"].stringValue
            myString = myString?.replacingOccurrences(of: "-", with: "")
            myString = myString?.replacingOccurrences(of: ".", with: "")
            if(!myString!.isEmpty && ((myString?.range(of: searchText)) != nil)){
                filterCustomers.append(custDict)
                continue;
            }
            
            myString = custDict["CustCnt_AltNum"].stringValue
            myString = myString?.replacingOccurrences(of: "-", with: "")
            myString = myString?.replacingOccurrences(of: ".", with: "")
            if(!myString!.isEmpty && ((myString?.range(of: searchText)) != nil)){
                filterCustomers.append(custDict)
                continue;
            }
            
            myString = custDict["CustCnt_HomeNum"].stringValue
            myString = myString?.replacingOccurrences(of: "-", with: "")
            myString = myString?.replacingOccurrences(of: ".", with: "")
            if(!myString!.isEmpty && ((myString?.range(of: searchText)) != nil)){
                filterCustomers.append(custDict)
                continue;
            }
        }
        
        self.customers = filterCustomers
        self.customerTableView.reloadData()
    }
    
    @objc func backButtonDidTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callFromCustomer(strCusNum:String){
        isFromCustomer = true
        strURL = strCusNum
        let backButton = UIBarButtonItem(image: UIImage(named: "back")!, style: .plain, target: self, action: #selector(backButtonDidTap))
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    func setMessage(strMsg:String){
        strLblFilter = strMsg
    }
    
    private func callService() {
        Utils.showHUD(view: self.view)
        let headers = [
            "Authorization": "Basic bWFuYWdlcjpkZW1vMjQ2OA=="
        ]
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { [weak self] (responseData) in
            
            Utils.hideHUD()
            if(responseData.result.value != nil){
                let r = responseData.result.value! as! [String:Any]
                let jsonAr = JSON(r["value"]!)
                self!.customers =  jsonAr.array!
                self!.origCustomers = self!.customers
                DispatchQueue.main.async {
                    self?.customerTableView.reloadData()
                }
            }
        }
    }
}

extension ContactsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let custDict:JSON = self.customers[indexPath.row] as! JSON
        var rowHeight:Int = 0
        
        var myString:String? = custDict["CustCnt_CellPhoneNum"].stringValue
        if(!myString!.isEmpty){
            rowHeight += 40
        }
        
        myString = custDict["CustCnt_PhoneNum"].stringValue
        if(!myString!.isEmpty){
            rowHeight += 40
        }
        
        myString = custDict["CustCnt_AltNum"].stringValue
        if(!myString!.isEmpty){
            rowHeight += 40
        }
        
        myString = custDict["CustCnt_HomeNum"].stringValue
        if(!myString!.isEmpty){
            rowHeight += 40
        }
        
        return CGFloat(rowHeight == 0 ? 40 : rowHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let customerCell: CustomerTableViewCell = tableView.dequeueReusableCell(withIdentifier: CustomerTableViewCell.identifier) as! CustomerTableViewCell
        
        let custDict:JSON = self.customers[indexPath.row] as! JSON
        customerCell.nameLabel.text = custDict["CustCnt_Name"].stringValue
        customerCell.idLabel.text = custDict["CustCnt_ContactTitle"].stringValue
        
        var cellString:String = ""
        var myString:String? = custDict["CustCnt_CellPhoneNum"].stringValue
        if(!myString!.isEmpty){
            cellString += myString! + " "
        }
        
        myString = custDict["CustCnt_PhoneNum"].stringValue
        if(!myString!.isEmpty){
            cellString += myString! + " "
        }
        
        myString = custDict["CustCnt_AltNum"].stringValue
        if(!myString!.isEmpty){
            cellString += myString! + " "
        }
        
        myString = custDict["CustCnt_HomeNum"].stringValue
        if(!myString!.isEmpty){
            cellString += myString! + " "
        }
        
        customerCell.grpText.text = cellString
        return customerCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

