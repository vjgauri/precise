
import UIKit

protocol MenuProtocol: class {
    func menuDidSelect()
}

class DashboardViewModel: NSObject,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    weak var delegate: MenuProtocol?
    
    let menuItems = DashboardGridMenu.getGridMenu(userType: .Employee)
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.frame.size.width
        return CGSize(width: collectionViewWidth/3, height: collectionViewWidth/3);
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dashboardCell: DashboardCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardCollectionViewCell.identifier, for: indexPath) as! DashboardCollectionViewCell
        
        dashboardCell.configureCell(menuItems[indexPath.row])
        return dashboardCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.menuDidSelect()
    }
}
