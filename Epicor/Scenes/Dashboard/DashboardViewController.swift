

import UIKit

class DashboardViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet var versionLabel: UILabel!
    @IBOutlet var userInfoBackgroundView: UIView!
    @IBOutlet var dateView: UIView!
    @IBOutlet var dateSelectionButton: UIButton!
    @IBOutlet var menuView: UIView!
    @IBOutlet var menuCollectionView: UICollectionView!
    let viewModel = DashboardViewModel()
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Home"
        
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setCollectionViewFlowLayout()
    }
    
    //MARK: UI Configuration
    func configureUI() {
        
        self.menuCollectionView.setCollectionViewCornerRadius(0.0)
        self.dateSelectionButton.setTitleColor(UIColor.navigationColor, for: .normal)
        self.menuView.setCornerRadius(4.0)
        self.userInfoBackgroundView.setbackgoundColor(UIColor.navigationColor.withAlphaComponent(0.6))
        self.dateView.setCornerRadius(4.0)
        self.versionLabel.cornerRadius(4.0)
        self.setBackgroundColor()
    }
}

//MARK: CollectionView Datasource and Delegate
extension DashboardViewController: MenuProtocol {
    
    func menuDidSelect() {
        let leadsController = self.storyboard?.instantiateViewController(withIdentifier: LeadsViewController.identifier) as! LeadsViewController
        self.navigationController?.pushViewController(leadsController, animated: true)
    }
    
    func setCollectionViewFlowLayout() {
        
        let collectionViewWidth = self.menuCollectionView.frame.size.width
        menuCollectionView.delegate = viewModel
        menuCollectionView.dataSource = viewModel
        viewModel.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: collectionViewWidth/3, height: collectionViewWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        menuCollectionView.collectionViewLayout = layout
    }
        
}
