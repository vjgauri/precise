//
//  TaskJobOperationViewController.swift
//  Epicor
//
//  Created by king on 4/28/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit
import SwiftyJSON

class TaskJobOperationViewController: UIViewController {
    
    @IBOutlet var jobIdTextField: UITextField!
    @IBOutlet var operationTextField: UITextField!
    @IBOutlet var descTextField: UITextField!
    @IBOutlet var priorityTextField: UITextField!
    @IBOutlet var taskAndOperationsTableView: UITableView!
    
    var sections: [TaskSection] = []
    var arrTitleForCell : [String] = ["Assigned Resources  >", "Materials / Parts  >"]
    var tasksAndOperations: [JSON] = []
    var jsonData:JSON?
    var jobId: String?
    @IBOutlet var btnCheck: UIButton!
    @IBAction func btnCheckClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Task / Job Operation"
        self.taskAndOperationsTableView.register(UINib(nibName: "TaskHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskHeaderTableViewCell")
        self.taskAndOperationsTableView.register(UINib(nibName: "TaskTableViewCell", bundle: nil), forCellReuseIdentifier: "taskCell")
        self.taskAndOperationsTableView.estimatedRowHeight = 50
        self.taskAndOperationsTableView.rowHeight = UITableView.automaticDimension
        self.taskAndOperationsTableView.tableFooterView = UIView()
        
        getDataForNewTask()
    }
    
    func getDataForNewTask() {
        
        Utils.showHUD(view: self.view)
        TaskService.shared.getNewTaskData(
            successHandler: { (result) in
                Utils.hideHUD()
                if result.count > 0 {
                    self.configureUI(data: result[0])
                }
        }) { (error) in
            Utils.hideHUD()
            print(error)
        }
    }
    
    func configureUI(data: JSON) {
       
        jsonData = data
        jobIdTextField.text = jobId!
        let description = data["JobOper_OpDesc"].stringValue
        if !description.isEmpty {
            descTextField.text = description
        }
    }
    
    
    func showAlertAndNavigateBack(title: String, jobId: String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func createTaskDidTap(_ sender: Any) {
    
        if jsonData != nil {
            
            let param: [String: Any] = [
                "JobHead_JobNum": jobId!,
                "JobOper_OprSeq": operationTextField.text!,
                "JobOper_OpCode": "\(jsonData!["JobOper_OpCode"])",
                "JobOper_OpDesc": descTextField.text!,
                "JobOper_OpComplete": true,
                "JobOper_AssemblySeq":"\(jsonData!["JobOper_AssemblySeq"])",
                "RowMod":"\(jsonData!["RowMod"])",
                "JobHead_Plant":"\(jsonData!["JobHead_Plant"])",
                "RowIdent": "\(jsonData!["RowIdent"])",
                "SysRowID": "\(jsonData!["SysRowID"])"
            ]
            
            Utils.showHUD(view: self.view)
            TaskService.shared.createNewTask(parameters: param,
                                             successHandler: { (result) in
                                                self.showAlertAndNavigateBack(title: "Task/Operation has been created successfully", jobId: "")
                                                Utils.hideHUD()
                                                
            }) { (error) in
                print(error)
                Utils.hideHUD()
            }
        }
    }

}

extension TaskJobOperationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitleForCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = arrTitleForCell[indexPath.row]
        cell.textLabel!.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        cell.textLabel?.textColor =  UIColor.navigationColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

