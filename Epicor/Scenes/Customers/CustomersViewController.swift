

import UIKit
import Alamofire
import SwiftyJSON

class CustomersViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet var customerTableView: UITableView!
    
    @IBOutlet weak var searchbar: UISearchBar!
    @IBAction func btnMenuDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    static let identifier = "CustomersViewController"
    var customers: [Any] = []
    var origCustomers:[Any] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Customers"
        searchbar.delegate = self
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.singleTap(sender:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
        callService()
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        self.searchbar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var filterCustomers: [Any] = []
        
        if(searchText.isEmpty){
            self.customers = self.origCustomers
            self.customerTableView.reloadData()
            return
        }
        
        for i in 0...self.origCustomers.count-1 {
            let custDict:JSON = self.origCustomers[i] as! JSON
            let custName:String = custDict["Customer_Name"].stringValue
            if(custName.range(of: searchText) != nil){
                filterCustomers.append(custDict)
            }
        }
        
        self.customers = filterCustomers
        self.customerTableView.reloadData()
    }
    private func callService() {
        /*
        Utils.showHUD(view: self.view)
        CustomerService.shared.getCustomers(afterDate: "",
                                            afterTime: "",
                                            successHandler: { (customers) in
                                                Utils.hideHUD()
                                                DispatchQueue.main.async {
                                                    self.cus
                                                    self?.customerTableView.reloadData()
                                                }
        }) { (error) in
            Utils.hideHUD()
        }
        
        */
        Utils.showHUD(view: self.view)
        
        let headers = [
            "Authorization": "Basic bWFuYWdlcjpkZW1vMjQ2OA=="
        ]
        
        Alamofire.request("https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/zCRM-Customers/?AfterDate=01%2F01%2F2000&AfterTime=0", method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { [weak self] (responseData) in
            
            Utils.hideHUD()
            if(responseData.result.value != nil){
                let r = responseData.result.value! as! [String:Any]
                let jsonAr = JSON(r["value"]!)
                self!.customers =  jsonAr.array!
                self?.origCustomers = self!.customers
                
                DispatchQueue.main.async {
                    self?.customerTableView.reloadData()
                }
            }
            
        }
        
    }
}

extension CustomersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let customerCell: CustomerTableViewCell = tableView.dequeueReusableCell(withIdentifier: CustomerTableViewCell.identifier) as! CustomerTableViewCell
        
        let custDict:JSON = self.customers[indexPath.row] as! JSON
        
        customerCell.nameLabel.text = custDict["Customer_Name"].stringValue
        customerCell.idLabel.text = custDict["Customer_CustID"].stringValue
        if custDict["CustGrup_GroupDesc"].stringValue.count != 0 {
            customerCell.grpText.text = custDict["CustGrup_GroupDesc"].stringValue
        } else {
            customerCell.grpText.text = ""
        }
        
        return customerCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let customerDetails: CustomerDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: CustomerDetailsViewController.identifier) as! CustomerDetailsViewController
        customerDetails.customerDetails = (self.customers[indexPath.row] as! JSON)
        customerTableView.deselectRow(at: indexPath, animated: true)
        navigationController?.pushViewController(customerDetails, animated: true)
    }
}

