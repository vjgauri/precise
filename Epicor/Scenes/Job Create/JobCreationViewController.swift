

import UIKit
import SwiftyJSON

protocol JobCreation: class {
    func jobDidCreate(jonNum: String)
}

class JobCreationViewController: UIViewController,UITextFieldDelegate {

    weak var delegate: JobCreation?
    
    @IBOutlet var dueDateTextField: UITextField!
    @IBOutlet var startDateTextField: UITextField!
    @IBOutlet var addressTextArea: UITextView!
    @IBOutlet var equipmentTextField: UITextField!
    @IBOutlet var descriptionTextField: UITextField!
    @IBOutlet var statusTextField: UITextField!
    @IBOutlet var contractTextField: UITextField!
    @IBOutlet var customerTextField: UITextField!
    @IBOutlet var jobIdTextField: UITextField!
    
    var data: JSON?
    
    let datePicker = UIDatePicker()
    var activeTextField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.datePickerMode = UIDatePicker.Mode.date
        
        self.title = "Create Job"
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(doneButtonDidTap))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelButtonDidTap))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        startDateTextField.inputAccessoryView = toolbar
        dueDateTextField.inputAccessoryView = toolbar
        startDateTextField.inputView = datePicker
        dueDateTextField.inputView = datePicker
        startDateTextField.delegate = self
        dueDateTextField.delegate = self
        
        configuerNavigationBar()
        getNewJobDetails()
    }
    
    func configuerNavigationBar() {
        let backButton = UIBarButtonItem(image: UIImage(named: "back")!, style: .plain, target: self, action: #selector(backButtonDidTap))
        let settings = UIBarButtonItem(image: UIImage(named: "SettingsBearing")!, style: .plain, target: self, action: #selector(commonSelector))
        let refresh = UIBarButtonItem(image: UIImage(named: "refresh")!, style: .plain, target: self, action: #selector(commonSelector))
        
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.rightBarButtonItems = [settings,refresh]
    }
    
    @objc func backButtonDidTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func commonSelector() {
        
    }
    
    @objc func doneButtonDidTap() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        
        if activeTextField != nil {
            if activeTextField == startDateTextField {
                startDateTextField.text = formatter.string(from: datePicker.date)
            } else if activeTextField == dueDateTextField {
                dueDateTextField.text = formatter.string(from: datePicker.date)
            }
        }
        
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == startDateTextField {
            activeTextField = startDateTextField
        } else if textField == dueDateTextField {
            activeTextField = dueDateTextField
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }

    
    @objc func cancelButtonDidTap() {
        self.view.endEditing(true)
    }
    
    func getNewJobDetails() {
        
        Utils.showHUD(view: self.view)
        JobsService.shared.getDataForNewJob(successHandler: { (result) in
            Utils.hideHUD()
            
            if result.count > 0 {
                self.configureUI(data: result[0])
                self.data = result[0]
            }
            
        }) { (error) in
            Utils.hideHUD()
            print(error)
        }
    }
    
    func configureUI(data: JSON) {
        
        let jobId = data["JobHead_JobNum"].stringValue
        if !jobId.isEmpty {
            jobIdTextField.text = jobId
        }
        
        let custName = data["JobHead_CustName"].stringValue
        if !custName.isEmpty {
            customerTextField.text = custName
        }
        
        let description = data["JobHead_PartDescription"].stringValue
        if !description.isEmpty {
            descriptionTextField.text = description
        }
        
        let eqipId = data["JobHead_EquipID"].stringValue
        if !eqipId.isEmpty {
            equipmentTextField.text = eqipId
        }
        
    }
    
    func trim(text: String) -> String {
        return text.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    @IBAction func createJobButtonDidTap(_ sender: Any) {
        
        if trim(text: descriptionTextField.text!).count == 0 {
            showAlert(title: "Please enter Description")
            return
        }
        if trim(text: startDateTextField.text!).count == 0 {
            showAlert(title: "Please enter Start Date")
            return
        }
        if trim(text: dueDateTextField.text!).count == 0 {
            showAlert(title: "Please enter Due Date")
            return
        }
        
        let custId =  data!["JobHead_CustID"].stringValue
        let custName = data!["JobHead_CustName"].stringValue
        let partNum = data!["JobHead_PartNum"].stringValue
        let startDate = toStringOtherFormat(d: startDateTextField.text!)
        let dueDate = toStringOtherFormat(d: dueDateTextField.text!)
        let plant = data!["JobHead_Plant"].stringValue
        let ium = data!["JobHead_IUM"].stringValue
        let prodCode = data!["JobHead_ProdCode"].stringValue
        let equipId = data!["JobHead_EquipID"].stringValue
        let rowMode = data!["RowMod"].stringValue
        let rowIdent = data!["RowIdent"].stringValue
        let sysRowId = data!["SysRowID"].stringValue
        
        let parameters: [String: Any] = ["JobHead_JobNum": jobIdTextField.text!,
        "JobHead_CustID": custId,
        "JobHead_CustName": custName,
        "JobHead_PartNum": partNum,
        "JobHead_PartDescription": descriptionTextField.text!,
        "JobHead_JobReleased": false,
        "JobHead_JobComplete": false,
        "JobHead_StartDate": startDate,
        "JobHead_DueDate": dueDate,
        "JobHead_Plant": plant,
        "JobHead_IUM": ium,
        "JobHead_ProdCode": prodCode,
        "JobHead_EquipID": equipId,
        "RowMod": rowMode,
        "RowIdent": rowIdent,
        "SysRowID": sysRowId]
        
        Utils.showHUD(view: self.view)
        JobsService.shared.createNewJob(parameters: parameters,
                                        successHandler: { (result) in
                                            Utils.hideHUD()
                                            print(result)
                                            if result.count > 0 {
                                                let jobDict: JSON = result[0] 
                                                let jobId = jobDict["JobHead_JobNum"].stringValue
                                                if jobId == self.jobIdTextField.text! {
                                                         self.showAlertAndNavigateBack(title: "Job has been created successfully", jobId: jobId)
                                                }
                                            }
                                            
        }) { (error) in
            print(error)
            Utils.hideHUD()
        }
        
    }
    func showAlertAndNavigateBack(title: String, jobId: String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
           self.delegate?.jobDidCreate(jonNum: jobId)
            self.navigationController?.popViewController(animated: true)
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func toStringOtherFormat(d:String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let date = dateFormatter.date(from: d)
        return (date?.toString(format: "yyyy-MM-dd'T'HH:mm:ss"))!
    }
    
}
