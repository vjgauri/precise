//
//  MenuViewController.swift
//  Epicor
//
//  Created by King on 27/04/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit
import SideMenuSwift

class Preferences {
    static let shared = Preferences()
    var enableTransitionAnimation = false
}
class MenuViewController: UIViewController {
    @IBOutlet var menuTableView: UITableView!
    
    let menuList = ["Customers"]
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMenuIdentifierForSideMenu()

    }
    func configureMenuIdentifierForSideMenu(){
        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "ScheduleViewController")
        }, with: "0")
        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "CustomerNavigationIdentifier")
        }, with: "2")
        
        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "contactNavController")
        }, with: "5")
        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "JobsNavigationIdentifier")
        }, with: "9")
        
        
        sideMenuController?.delegate =  self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        menuTableView.delegate = self
        menuTableView.dataSource = self
        
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource, SideMenuControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.menuOptionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuCell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.identifier) as! MenuTableViewCell
        menuCell.menuTitleLabel.text =  Constants.menuOptionList[indexPath.row]
        return menuCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            sideMenuController?.setContentViewController(with: "0", animated: Preferences.shared.enableTransitionAnimation)
            sideMenuController?.hideMenu()
            break
        case 2:
            sideMenuController?.setContentViewController(with: "2", animated: Preferences.shared.enableTransitionAnimation)
            sideMenuController?.hideMenu()
            break
        case 5:
            sideMenuController?.setContentViewController(with: "5", animated: Preferences.shared.enableTransitionAnimation)
            sideMenuController?.hideMenu()
            break
        case 9:
            sideMenuController?.setContentViewController(with: "9", animated: Preferences.shared.enableTransitionAnimation)
            sideMenuController?.hideMenu()
            break
        
        default:
            break
        }
        
        
        
    }
    
}
