
import Foundation

//
// MARK: - Section Data Structure
//
public struct Item {
    var name: String
    var detail: String
    
    public init(name: String, detail: String) {
        self.name = name
        self.detail = detail
    }
}

public struct Section {
    var name: String
    var items: [Item]
    var collapsed: Bool
    
    public init(name: String, items: [Item], collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

public var sectionsData: [Section] = [
    
    Section(name: "Contacts", items: [
        Item(name: "Name", detail: "Position"),
    ]),
    Section(name: "Jobs", items: [
        Item(name: "iPad Pro", detail: "Position"),
        Item(name: "Accessories", detail: "")
    ]),
    Section(name: "Calls", items: [
    ]),
    Section(name: "Cases", items: [
    ]),
    Section(name: "Tasks", items: [
        ]),
    Section(name: "Ship To / Site Addresses", items: [
        ]),
    
    Section(name: "Price Lists", items: [
    ]),
    Section(name: "Contracts", items: [
        ]),
    Section(name: "Orders", items: [
        ]),
    Section(name: "Projects", items: [
    ]),
    Section(name: "Invoices", items: [
        ]),
]
