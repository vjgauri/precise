

import UIKit
import Alamofire
import SwiftyJSON
import Floaty

class CustomerDetailsViewController: UIViewController {

    var sections = sectionsData
    static let identifier = "CustomerDetailsViewController"
    var customerDetails: JSON?
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var customerDetailsTableView: UITableView!
    @IBOutlet var groupLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    var customers: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Customer Details"
        // Auto resizing the height of the cell
        customerDetailsTableView.estimatedRowHeight = 44.0
        customerDetailsTableView.rowHeight = UITableView.automaticDimension
        customerDetailsTableView.register(UINib(nibName: "CustJobsTableViewCell", bundle: nil), forCellReuseIdentifier: "CustJobsTableViewCell")
        self.configuerNavigationBar()
        self.configureUI()
        
        let floaty = Floaty()
        floaty.overlayColor = UIColor.gray.withAlphaComponent(0.9)
        floaty.buttonColor = UIColor.orange
        floaty.itemTitleColor = UIColor.orange
        floaty.itemShadowColor = UIColor.clear
        floaty.plusColor = UIColor.white
        
        floaty.itemImageColor = UIColor.clear
        floaty.itemSpace = 20
        floaty.itemSize = 30
        floaty.buttonShadowColor = UIColor.clear
        
        floaty.addItem("New Order", icon: UIImage(named: "orangearrow"))
        floaty.addItem("New Quote", icon: UIImage(named: "orangearrow"))
        floaty.addItem("New Ship To/Site", icon: UIImage(named: "orangearrow"))
        floaty.addItem("New Task", icon: UIImage(named: "orangearrow"))
        floaty.addItem("New Case", icon: UIImage(named:"orangearrow")!)
        floaty.addItem("New Call Log Entry", icon: UIImage(named:"orangearrow")!)
        floaty.addItem("New Contact", icon: UIImage(named:"orangearrow")!)
        floaty.addItem("Edit Customer", icon: UIImage(named:"orangearrow")!)
        self.view.addSubview(floaty)
        
    }
    
    func configuerNavigationBar() {
        let backButton = UIBarButtonItem(image: UIImage(named: "back")!, style: .plain, target: self, action: #selector(backButtonDidTap))
        let settings = UIBarButtonItem(image: UIImage(named: "SettingsBearing")!, style: .plain, target: self, action: #selector(commonSelector))
        let refresh = UIBarButtonItem(image: UIImage(named: "refresh")!, style: .plain, target: self, action: #selector(commonSelector))
        
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.rightBarButtonItems = [settings,refresh]
    }
    
    @objc func commonSelector() {
        
    }
    
    @objc func backButtonDidTap() {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func toStringOtherFormat(d:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: d)
        return (date?.toString(format: "dd MMM yyyy"))!
    }
    
    func configureUI() {
        
        guard let data = customerDetails else { return }
        
        self.idLabel.text = data["Customer_CustID"].stringValue
        self.nameLabel.text = data["Customer_Name"].stringValue
        if data["CustGrup_GroupDesc"].stringValue.count != 0 {
            self.groupLabel.text = data["CustGrup_GroupDesc"].stringValue
        } else {
            self.groupLabel.text = ""
        }
                
        var primaryAddress:String = ""
        let customerAddress1 = data["Customer_Address1"].stringValue
        let customerAddress2 = data["Customer_Address2"].stringValue
        let customerAddress3 = data["Customer_Address3"].stringValue
        let customerCity = data["Customer_City"].stringValue
        let CustomerState = data["Customer_State"].stringValue
        let CustomerZip = data["Customer_Zip"].stringValue
        
        if !customerAddress1.isEmpty{
            primaryAddress += customerAddress1
        }
        if !customerAddress2.isEmpty{
            primaryAddress += ",\n" + customerAddress2
        }
        if !customerAddress3.isEmpty{
            primaryAddress += ",\n" + customerAddress3
        }
        if !customerCity.isEmpty{
            primaryAddress += ",\n" + customerCity
        }
        if !CustomerState.isEmpty{
            primaryAddress += ", " + CustomerState
        }
        
        if !CustomerZip.isEmpty{
            primaryAddress += " " + CustomerZip
        }
        addressLabel.text = primaryAddress
        
        // Populate section here for Contacts
        Utils.showHUD(view: self.view)
        let headers = [
            "Authorization": "Basic bWFuYWdlcjpkZW1vMjQ2OA=="
        ]
    Alamofire.request("https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/zCRM-CustCnt/?AfterDate=01%2F01%2F1970&AfterTime=0&$filter=CustCnt_CustNum%20eq%20" + data["Customer_CustNum"].stringValue, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { [weak self] (responseData) in
            
            Utils.hideHUD()
            if(responseData.result.value != nil){
                let r = responseData.result.value! as! [String:Any]
                let jsonAr = JSON(r["value"]!)
                self!.customers =  jsonAr.array!
                if(self!.customers.count <= 0){
                    return
                }
                DispatchQueue.main.async {
                    var items:[Item] = []
                    items.append(Item(name: "Name", detail: "Position"))
                    for i in 0...self!.customers.count-1 {
                        let custDict:JSON = self!.customers[i] as! JSON
                        let custName:String = custDict["CustCnt_Name"].stringValue
                        let custDes:String = custDict["CustCnt_ContactTitle"].stringValue
                        let it:Item = Item(name: custName, detail: custDes)
                        items.append(it)
                    }
                    let sec:Section = Section(name: "Contacts", items: items, collapsed: false)
                    self!.sections[0] = sec
                    self!.customerDetailsTableView.reloadData()
                }
            }
        }
        
        // Fetch Jobs
        Alamofire.request("https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobList/?$filter=JobHead_CustID%20eq%20'" + data["Customer_CustID"].stringValue + "'", method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { [weak self] (responseData) in
            
            Utils.hideHUD()
            if(responseData.result.value != nil){
                let r = responseData.result.value! as! [String:Any]
                let jsonAr = JSON(r["value"]!)
                self!.customers =  jsonAr.array!
                if(self!.customers.count <= 0){
                    return
                }
                DispatchQueue.main.async {
                    var items:[Item] = []
                    items.append(Item(name: "Name", detail: "Position"))
                    for i in 0...self!.customers.count-1 {
                        let custDict:JSON = self!.customers[i] as! JSON
                        let custName:String = custDict["JobHead_JobNum"].stringValue
                        let custDes:String = custDict["JobHead_PartDescription"].stringValue
                        var dueDateStr:String = ""
                        let dueDate = custDict["JobHead_DueDate"].stringValue
                        if !dueDate.isEmpty {
                            // let Converted_duedate = self.getDate(fromStr: dueDate)
                            dueDateStr = self!.toStringOtherFormat(d: dueDate)
                        }
                        var jobStatusStri:String = ""
                        let jobStatus = custDict["JobHead_Status"].stringValue
                        if jobStatus.isEmpty {
                            jobStatusStri = "Open"
                        }
                        let it:Item = Item(name: custName, detail: custDes, due: dueDateStr, status: jobStatusStri)
                        items.append(it)
                    }
                    let sec:Section = Section(name: "Jobs", items: items, collapsed: false)
                    self!.sections[1] = sec
                    self!.customerDetailsTableView.reloadData()
                }
            }
        }
    }
    
}

extension CustomerDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].collapsed ? 0 : sections[section].items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
            let item: Item = sections[indexPath.section].items[indexPath.row]
            let jobId = item.name
            let jobDetailsVC: JobDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: JobDetailsViewController.identifier) as! JobDetailsViewController
            
            jobDetailsVC.jobNum = jobId
            tableView.deselectRow(at: indexPath, animated: true)
            self.navigationController?.pushViewController(jobDetailsVC, animated: true)
        }
        
    }
    
    // Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            
            let jobCell: CustJobsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CustJobsTableViewCell") as! CustJobsTableViewCell
            if(indexPath.row == 0){
                jobCell.jobID.text = "Job #"
                jobCell.jobID.textColor = UIColor.blue
                jobCell.jobDesc.text = "Description"
                jobCell.jobDesc.textColor = UIColor.blue
                jobCell.jobDue.text = "Due"
                jobCell.jobDue.textColor = UIColor.blue
                jobCell.jobStatus.text = "Status"
                jobCell.jobStatus.textColor = UIColor.blue
            }else{
                let item: Item = sections[indexPath.section].items[indexPath.row]
                jobCell.jobID.text = item.name
                jobCell.jobDesc.text = item.detail
                jobCell.jobDue.text = item.due
                jobCell.jobStatus.text = item.status
                
            }
            return jobCell
        
        } else {
            
            if(indexPath.row == 0) {
                let cell: CollapseHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapseHeaderTableViewCell ??
                    CollapseHeaderTableViewCell(style: .default, reuseIdentifier: "cell")
                
                let item: Item = sections[indexPath.section].items[indexPath.row]
                
                cell.nameLabel.text = item.name
                cell.detailLabel.text = item.detail
                return cell
            } else {
                let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
                    CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
                
                let item: Item = sections[indexPath.section].items[indexPath.row]
                
                cell.nameLabel.text = item.name
                cell.detailLabel.text = item.detail
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = sections[section].name
        header.arrowLabel.text = sections[section].collapsed ? ">" : ">"
        header.setCollapsed(sections[section].collapsed)
        
        header.section = section
        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}

extension CustomerDetailsViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        self.customerDetailsTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    func detailSection(_ header: CollapsibleTableViewHeader, section: Int) {
        
        if(section == 0) {
            // Contacts - Move to next screen with clickable value
            let url:String = "https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/zCRM-CustCnt/?AfterDate=01%2F01%2F1970&AfterTime=0&$filter=CustCnt_CustNum%20eq%20" + customerDetails!["Customer_CustNum"].stringValue
            
            let cv:ContactsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
            cv.callFromCustomer(strCusNum: url)
            cv.setMessage(strMsg: "For Customer: " + customerDetails!["Customer_Name"].stringValue + " (" + customerDetails!["Customer_CustID"].stringValue + ")")
            
            navigationController?.pushViewController(cv, animated: true)
        }
        
        if(section == 1){
            let url:String = "https://crmdemo.precisebusiness.com.au/ERP10/api/v1/BaqSvc/PbsMPSJobList/?$filter=JobHead_CustID%20eq%20'" + customerDetails!["Customer_CustID"].stringValue + "'"
            let jb:JobsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobsViewController") as! JobsViewController
            jb.callFromCustomer(strCusNum: url)
            jb.setMessage(strMsg: "For Customer: " + customerDetails!["Customer_Name"].stringValue + " (" + customerDetails!["Customer_CustID"].stringValue + ")")
            navigationController?.pushViewController(jb, animated: true)
            
        }
    }
}
