

import Foundation

public struct TaskAndOperation {
    var description: String
    var assignedTo: String
    var completed: Bool
    
    public init(description: String, assignedTo: String, completed: Bool) {
        self.description = description
        self.assignedTo = assignedTo
        self.completed = completed
    }
}

public struct TaskSection {
    var name: String
    var items: [TaskAndOperation]
    var collapsed: Bool
    
    public init(name: String, items: [TaskAndOperation], collapsed: Bool = false) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

public var TasksectionsData: [TaskSection] = [
    
    TaskSection(name: "Task / Operations", items: [
        TaskAndOperation(description: "Description", assignedTo: "Assigned To", completed: false),
        TaskAndOperation(description: "Description", assignedTo: "Assigned To", completed: false)
        ]),
    TaskSection(name: "Materials / Parts", items: [
        
        ])
]
