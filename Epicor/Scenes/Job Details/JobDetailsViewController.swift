
import UIKit
import SwiftyJSON
import Floaty

class JobDetailsViewController: UIViewController, JobCreation {

    
    @IBOutlet var jobIdTextField: UITextField!
    @IBOutlet var customerTextField: UITextField!
    @IBOutlet var contractTextField: UITextField!
    @IBOutlet var statusTextField: UITextField!
    @IBOutlet var jobDescriptionTextField: UITextField!
    @IBOutlet var equipmentTextField: UITextField!
    @IBOutlet var primaryAddressTextArea: UITextView!
    @IBOutlet var dueDateTextField: UITextField!
    @IBOutlet var startDateTextField: UITextField!
    @IBOutlet var taskAndOperationsTableView: UITableView!
    
    var sections: [TaskSection] = []
    
    var tasksAndOperations: [JSON] = []
    static let identifier = "JobDetailsViewController"
    var jobNum: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.taskAndOperationsTableView.register(UINib(nibName: "TaskHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskHeaderTableViewCell")
        self.taskAndOperationsTableView.register(UINib(nibName: "TaskTableViewCell", bundle: nil), forCellReuseIdentifier: "taskCell")
        self.taskAndOperationsTableView.estimatedRowHeight = 50
        self.taskAndOperationsTableView.rowHeight = UITableView.automaticDimension
        self.title = "Job"
        
        
        let floaty = Floaty()
        floaty.overlayColor = UIColor.gray.withAlphaComponent(0.9)
        floaty.buttonColor = UIColor.orange
        floaty.itemTitleColor = UIColor.orange
        floaty.itemShadowColor = UIColor.clear
        floaty.plusColor = UIColor.white
        
        floaty.itemImageColor = UIColor.clear
        floaty.itemSpace = 20
        floaty.itemSize = 30
        floaty.buttonShadowColor = UIColor.clear
        
        floaty.addItem("Generate Order", icon: UIImage(named:"orangearrow")!)
        floaty.addItem("Generate Quote", icon: UIImage(named:"orangearrow")!)
        floaty.addItem("Add Material/Part", icon: UIImage(named:"orangearrow")!)
        floaty.addItem("Add Task/Operation", icon: UIImage(named:"orangearrow")!, handler: { item in
            let taskVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskJobOperationViewController") as! TaskJobOperationViewController
            taskVC.jobId = self.jobNum!
            self.navigationController?.pushViewController(taskVC, animated: true)
            floaty.close()
        })
        floaty.addItem("Edit Job Header", icon: UIImage(named:"orangearrow")!)
        floaty.addItem("Create New Job", icon: UIImage(named: "orangearrow")!, handler: { item in
            let jobCreationViewController: JobCreationViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobCreationViewController") as! JobCreationViewController
            jobCreationViewController.delegate = self

            self.navigationController?.pushViewController(jobCreationViewController, animated: true)
            floaty.close()
        })
        self.view.addSubview(floaty)
        configuerNavigationBar()
        setAllFieldsReadonly()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getJobDetails(jobId: jobNum!)
    }
    
    
    func configuerNavigationBar() {
        let backButton = UIBarButtonItem(image: UIImage(named: "back")!, style: .plain, target: self, action: #selector(backButtonDidTap))
        let settings = UIBarButtonItem(image: UIImage(named: "SettingsBearing")!, style: .plain, target: self, action: #selector(commonSelector))
        let refresh = UIBarButtonItem(image: UIImage(named: "refresh")!, style: .plain, target: self, action: #selector(commonSelector))
        
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.rightBarButtonItems = [settings,refresh]
    }
    
    @objc func backButtonDidTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func commonSelector() {
        
    }
    
    func jobDidCreate(jonNum: String) {
        self.getJobDetails(jobId: jonNum)
    }

    private func setAllFieldsReadonly(){
        jobIdTextField.isEnabled = false
        customerTextField.isEnabled = false
        contractTextField.isEnabled = false
        statusTextField.isEnabled = false
        jobDescriptionTextField.isEnabled = false
        equipmentTextField.isEnabled = false
        primaryAddressTextArea.isEditable = false
        dueDateTextField.isEnabled = false
        startDateTextField.isEnabled = false
    }
    
    func getJobDetails(jobId: String) {
        
        Utils.showHUD(view: self.view)
        JobsService.shared.getJobDetails(
            jobNumber: jobId,
            successHandler: { (result) in
                Utils.hideHUD()
                if result.count > 0 {
                    self.configureUI(data: result[0])
                }
                self.getTasksAndOperations()
        }) { (error) in
            Utils.hideHUD()
            print(error)
            self.getTasksAndOperations()
        }
    }
    
    func getTasksAndOperations() {
        
        JobsService.shared.getTasksAndOperations(
            jobNumber: jobNum!,
            successHandler: { (result) in
                self.getSectionData(data: result)
        }) { (error) in
            print(error)
        }
    }
    
    func getSectionData(data: [JSON]) {
        
        var items: [TaskAndOperation] = []
        if(data.count >= 1){
            items.append(TaskAndOperation(description: "Description", assignedTo: "Assigned To", completed: false))
        }
        data.forEach { (taskDict) in

            var description = ""
            let desc = taskDict["JobOper_OpDesc"].stringValue
            if !desc.isEmpty {
                description = desc
            } else {
                description = ""
            }
            let isComplted = taskDict["JobOper_OpComplete"].boolValue
            let task = TaskAndOperation(description: description, assignedTo: "", completed: isComplted)
            items.append(task)
        }
        
        sections = [TaskSection(name: "Task / Operations", items: items),
                    TaskSection(name: "Materials / Parts", items: [
            ])]
        
        self.taskAndOperationsTableView.reloadData()
        
    }
    
    func configureUI(data: JSON) {
        
        let jobId = data["JobHead_JobNum"].stringValue
        if !jobId.isEmpty {
            jobIdTextField.text = "\(jobId)"
        }
        let custName = data["JobHead_CustName"].stringValue
        if !custName.isEmpty {
            customerTextField.text = "\(custName)"
        }
        let jobDesciption = data["JobHead_PartDescription"].stringValue
        if !jobDesciption.isEmpty {
            jobDescriptionTextField.text = "\(jobDesciption)"
        }
        let equipmentId = data["JobHead_EquipID"].stringValue
        if !equipmentId.isEmpty {
            equipmentTextField.text = "\(equipmentId)"
        }
        let startDate = data["JobHead_StartDate"].stringValue
        if !startDate.isEmpty {
            startDateTextField.text = self.toStringOtherFormat(d: startDate)
        }
        let dueDate = data["JobHead_DueDate"].stringValue
        if !dueDate.isEmpty {
            dueDateTextField.text = self.toStringOtherFormat(d: dueDate)
        }
    }
    
    func toStringOtherFormat(d:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: d)
        return (date?.toString(format: "dd MMM yyyy"))!
    }
    
}

extension JobDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].collapsed ? 0 : sections[section].items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    // Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                let cell: TaskHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TaskHeaderTableViewCell") as! TaskHeaderTableViewCell
                 sections[indexPath.row].collapsed = false
                return cell
            } else {
                let cell: TaskTableViewCell = tableView.dequeueReusableCell(withIdentifier: "taskCell") as! TaskTableViewCell
                let task = sections[indexPath.section].items[indexPath.row]
                cell.descriptionLabel.text = task.description
                cell.assignedToLabel.text = task.assignedTo
                if task.completed {
                    cell.completedImageView.image = UIImage(named: "Checked-Update")
                } else {
                   cell.completedImageView.image = UIImage(named: "Unchecked-Update")
                }
                return cell
            }
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = sections[section].name
        header.arrowLabel.text = sections[section].collapsed ? ">" : ">"
       // header.setCollapsed(sections[section].collapsed)
         header.setCollapsed(true)
        
        header.section = section
        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension JobDetailsViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
    }
    
    func detailSection(_ header: CollapsibleTableViewHeader, section: Int) {
    /*    let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        self.taskAndOperationsTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
 */
        
    }
}





