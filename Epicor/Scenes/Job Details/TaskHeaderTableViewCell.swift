//
//  TaskHeaderTableViewCell.swift
//  Epicor
//
//  Created by King on 28/04/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit

class TaskHeaderTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
