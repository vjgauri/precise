//
//  TaskTableViewCell.swift
//  Epicor
//
//  Created by King on 28/04/19.
//  Copyright © 2019 com.Dogratech. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    @IBOutlet var completedImageView: UIImageView!
    @IBOutlet var assignedToLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
