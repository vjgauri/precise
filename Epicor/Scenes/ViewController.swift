

import UIKit
import SkyFloatingLabelTextField
import SwiftMessageBar


class ViewController: UIViewController {

    @IBOutlet weak var vwCenter: UIView!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet weak var txtPasword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var createAccount: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        configureUI()
        
        txtEmail.text = "brett.lee@academicservices.com"
        txtPasword.text = "demo2468"
    }
    
    func configureUI() {
        self.loginButton.setCornerRadius(5)
        createAccount.setCornerRadius(5)
        self.vwCenter.setCornerRadius(10)
        txtEmail.keyboardType = .emailAddress
        txtEmail.text = ""
        txtPasword.text = ""
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        /*
        if txtPasword.text == "" || txtEmail.text == "" {
         //   SwiftMessageBar.showMessage(withTitle: "Error", message: "Please enter valid values", type: .error)
            showAlert(title: "Error", msg: "Please enter valid values")
            
            return
        }
        
        if !(txtEmail.text?.isValidEmail())!{
          //  SwiftMessageBar.showMessage(withTitle: "Invalid email", message: "Email entered is not valid one.", type: .error)
            showAlert(title: "Invalid email", msg: "Email entered is not valid one.")
            return
        }
        
        if (txtEmail.text != "Brett.Lee@academyservices.com") || (txtPasword.text != "demo2468"){
            showAlert(title: "Error", msg: "Incorrect credentials.");
            return
        }
        */
        let scheduleViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ScheduleViewController.identifier) as! ScheduleViewController
        self.navigationController?.pushViewController(scheduleViewController, animated: true)
    }
    
    func showAlert(title : String, msg : String ){
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

