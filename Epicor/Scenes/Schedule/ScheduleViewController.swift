
import UIKit
import JZCalendarWeekView
import SideMenuSwift
import SwiftyJSON

class ScheduleViewController: UIViewController {
    
    @IBOutlet var jobTitleLabel: UILabel!
    @IBOutlet var menuButton: UIBarButtonItem!
    @IBOutlet var scheduleCalenderView: DefaultWeekView!
    @IBOutlet var jobsTableView: UITableView!
    static let identifier = "ScheduleViewController"
    let viewModel = DefaultViewModel()
    var jobs:[Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        jobsTableView.delegate = self
        jobsTableView.dataSource = self
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController!.navigationBar.setNavigationGradientBackground()
        // Basic setup
        scheduleCalenderView.setupCalendar(numOfDays: 7,
                                       setDate: Date(),
                                       allEvents: viewModel.eventsByDate,
                                       scrollType: .pageScroll)
        scheduleCalenderView.layer.borderColor = UIColor.lightGray.cgColor
        scheduleCalenderView.layer.borderWidth =  1.0
        scheduleCalenderView.updateFlowLayout(JZWeekViewFlowLayout(hourHeight: 50, rowHeaderWidth: 50, columnHeaderHeight: 50, hourGridDivision: JZHourGridDivision.noneDiv))
        
        getJobs()
        
    }
    
    func getJobs() {
        
        Utils.showHUD(view: self.view)
        JobsService.shared.getJobs(url: "", successHandler: { (json) in
            DispatchQueue.main.async {
                Utils.hideHUD()
                self.jobs = json
                self.jobsTableView.reloadData()
            }
        }) { (error) in
            Utils.hideHUD()
            print(error)
        }
    }
    
    @IBAction func menuButtonDidTap(_ sender: Any) {
        self.sideMenuController?.revealMenu()
    }
}

extension ScheduleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let jobCell: JobTableViewCell = tableView.dequeueReusableCell(withIdentifier: JobTableViewCell.identifier) as! JobTableViewCell
        
        let jobDict: JSON = self.jobs[indexPath.row] as! JSON
        print(jobDict)
        let jobId = jobDict["JobHead_JobNum"].stringValue
        if !jobId.isEmpty {
            jobCell.jobIdLabel.text = "ID: \(jobId)"
        }
        let jobDescription = jobDict["JobHead_PartDescription"].stringValue
        if !jobDescription.isEmpty {
            jobCell.jobDescriptionLabel.text = jobDescription
        }
        let dueDate = jobDict["JobHead_DueDate"].stringValue
        if !dueDate.isEmpty {
            jobCell.dueDateLabel.text = "Due: \(toStringOtherFormat(d: dueDate))"
        } else {
            jobCell.dueDateLabel.text = "Due: "
        }
        let jobStatus = jobDict["JobHead_Status"].stringValue
        if !jobId.isEmpty {
            jobCell.status.text = "Status: Open"
        } else {
            jobCell.status.text = "Status: Open"
        }
        
        
        return jobCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let jobDict: JSON = self.jobs[indexPath.row] as! JSON
        let jobId = jobDict["JobHead_JobNum"].stringValue
        let jobDetailsVC: JobDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: JobDetailsViewController.identifier) as! JobDetailsViewController
        
        jobDetailsVC.jobNum = jobId
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(jobDetailsVC, animated: true)
        
    }

    func toStringOtherFormat(d:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: d)
        return (date?.toString(format: "dd MMM yyyy"))!
    }
    
}
