
import UIKit

class LeadsViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet var leadsTableView: UITableView!
    
    //MARK: Properties
    static let identifier = "LeadsViewController"
    private let viewModel = LeadsViewModel()
    lazy var searchBar: UISearchBar = {
       return UISearchBar()
    }()
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Contracts"
        
        leadsTableView.backgroundView = UIView()
        leadsTableView.delegate = viewModel
        leadsTableView.dataSource = viewModel
        configureNavigationBar()
        
        fetchContracts()
    }
    
    func fetchContracts() {
        ContractService.shared.getContracts(userId: "1",
                                            contractStatus: "active",
                                            successHandler: { (contracts) in
                                                print(contracts)
        }) { (error) in
            
        }
    }
    
    func configureNavigationBar() {
        let menuBarButton = UIBarButtonItem(image: UIImage(named: "menu"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(menuButtonDidTap))
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "search"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(searchButtonDidTap))
        let backBarButton = UIBarButtonItem(image: UIImage(named: "back"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(backButtonDidTap))
        self.navigationItem.leftBarButtonItems = [menuBarButton,backBarButton]
        self.navigationItem.rightBarButtonItem = searchBarButton
    }
    
    @objc func menuButtonDidTap() {
    
    }
    
    @objc func searchButtonDidTap() {
        searchBar.sizeToFit()
        self.navigationItem.titleView = searchBar
    }
    
    @objc func backButtonDidTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
