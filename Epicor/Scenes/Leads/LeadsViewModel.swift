

import UIKit

class LeadsViewModel: NSObject, UITableViewDelegate, UITableViewDataSource {

    let sections = ["pizza", "deep dish pizza", "calzone"]
    let items = [["Margarita", "BBQ Chicken", "Peproni"], ["sausage", "meat lovers", "veggie lovers"], ["sausage", "chicken pesto", "prawns & mashrooms"]]
  
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let leadsCell: LeadsTableViewCell = tableView.dequeueReusableCell(withIdentifier: LeadsTableViewCell.identifier) as! LeadsTableViewCell
        leadsCell.leadTitleLabel.text = items[indexPath.section][indexPath.row]
        return leadsCell
    }
    
    

}
